<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use Session;


class PesertaController extends Controller
{
    //use AuthenticatesUsers;

    public function simpan_maklumat(Request $request)
    {
        //dd($request->all());
        $request->validate(
            [
                'nama' => 'required|min:5|string',
                'emel' => 'required|email',
                'katalaluan' => 'required|min:8|max:12'
            ],
            [
                'nama.required' => 'Sila masukkan nama anda!!!',
                'nama.min' => 'Minimum nama adalah 5 huruf',
                'nama.string' => 'Hanya huruf dibenarkan!!!',
                'emel.required' => 'Sila masukkan emel!!!',
                'emel.email' => 'Emel tidak lengkap!!!',
                'katalaluan.required' => 'Sila masukkan katalaluan!!!',
                'katalaluan.min' => 'Panjang minimum katalaluan adalah 8 aksara!!!',
                'katalaluan.max' => 'Panjang maksimum katalaluan adalah 12 aksara!!!'
            ]
        );

        $user = new User();
        $user->create([
            'nama' /*namafield dalam DB*/ => $request->nama /*request data dari form*/,
            'emel' => $request->emel,
            'katalaluan' => Hash::make($request->katalaluan)
        ]);

        return redirect()->route('utama');
    }

    public function authenticate(Request $request){
        //dd($request->all());



        request()->validate(
            [
                'emel' => 'required',
                'katalaluan' => 'required',
            ]);

        //$credentials = $request->only('emel', 'katalaluan');

        $credentials = [
            'emel' => $request->emel, 
            'password'=> $request->katalaluan
        ];

        
        if(Auth::attempt($credentials))
        {
            //dd($credentials);
            $request->session()->regenerate();

            return redirect()->intended('/dashboard/pengguna');
        }
        else
        {
            return redirect()->back();
        }
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect()->route('utama');
    }

    public function senarai_pengguna()
    {
        $senarai_pengguna = User::query()->get(); 
        //get() dapatkan senarai data dalam DB
        //first() dapatkan row pertama dari DB

        return view('pengguna.senarai_pengguna', compact('senarai_pengguna'));
    }

    public function kemaskini_pengguna($id)
    {
        //dd(decrypt($id));

        $semak_pengguna = User::query()->where('id', decrypt($id))->first();

        return view('pengguna.kemaskini_pengguna', compact('semak_pengguna'));
    }
}

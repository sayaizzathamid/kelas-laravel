<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    //php artisan make:controller HomeController -r
    //command untuk mewujudkan HomeController beserta fungsi CRUD asas
    //Create - C
    //Read - R
    //Update - U
    //Delete - D
    public function master_template() //nama function perlu sama dekat web.php
    {
        return view('layouts.master'); //master adalah nama file dalam folder layouts
    }

    public function index()
    {
        $nama = "Mohamad Izzat Hamid";
        $umur = "30 Tahun";
        $emel = "mdizzat@intanbk.intan.my";

        $user = User::query()->first();

        //dd($user); //dump&die

        return view('welcome', compact('nama','umur','emel'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): Response
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): RedirectResponse
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): RedirectResponse
    {
        //
    }
}

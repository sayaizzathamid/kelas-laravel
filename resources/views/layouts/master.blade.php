<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BOOKING SYSTEM</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <script type="text/javascript"></script>
    <style type="text/css"></style> <!--kita nak customize paparan teks/image/warna-->
    @stack('style') <!--untuk customize syle/css dari page lain-->

</head>

<body>
    @yield('content') <!--panggil content dari page lain-->
    <script type="text/javascript"> //untuk kod javascript
        
    </script>
    @stack('script') <!--untuk customize javascript dari page lain-->
</body>
</html>
@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h2>Kemaskini Pengguna</h2>
				</div>
				<div class="card-body">
					<div class="col-lg-12">
						<form action="" method="POST">
							<label>Nama Pengguna</label>
							<input type="text" class="form-control" value="{{ $semak_pengguna->nama }}" name="nama">
							<label>Emel</label>
							<input type="email" class="form-control" name="emel" value="{{ $semak_pengguna->emel }}">
							<br>
							<button class="btn btn-success btn-block">
								<i class="fa fa-save"></i> Kemaskini
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
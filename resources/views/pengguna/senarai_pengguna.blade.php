@extends('layouts.master')
@section('content')
	<div class="container">
		<div class="col-lg-12">
			<div class="card">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Bil.</th>
								<th>Nama</th>
								<th>Emel</th>
								<th>Tindakan</th>
							</tr>
						</thead>
						<tbody>
							@php
								$bil = 1;
							@endphp
							@foreach($senarai_pengguna as $sp)
								<tr>
									<td>{{ $bil++ }}</td>
									<td>{{ $sp->nama }}</td>
									<td>{{ $sp->emel }}</td>
									<td>
										<a href="{{ route('edit.pengguna', [encrypt($sp->id)]) }}">
											<button class="btn btn-primary">
												<i class="fa fa-edit"></i>
											</button>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
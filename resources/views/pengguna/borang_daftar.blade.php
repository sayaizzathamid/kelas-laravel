@extends('layouts.master')
@section('content')
<!-- <div class="row">
	<div class="col-lg-12" style="margin-left:30%; margin-right: 30%;">
		<img src="{{-- asset('img/jata.png') --}}" style="width: 10%; height: ;">
	</div>
</div> -->
<div class="container">
	<center>
		<img src="{{ asset('img/jata.png')}}" style="width: 20%; padding-top: 5%;">
	</center>
	<div class="col-lg-12" style="margin-top: 5%;">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox ">
					<div class="ibox-title">
						<h5>Pendaftaran Pengguna Baru</h5>
						<!-- <div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="#" class="dropdown-item">Config option 1</a>
								</li>
								<li><a href="#" class="dropdown-item">Config option 2</a>
								</li>
							</ul>
							<a class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div> -->
					</div>
					<div class="ibox-content">
						@if($errors->any())
							@foreach($errors->all() as $error)
								<div class="alert alert-danger">
									{{ $error }}
								</div>
							@endforeach
						@endif
						<form action="{{ route('simpan.maklumat') }}" method="POST">
							@csrf
							<p>Sila lengkapkan maklumat dibawah.</p>
							<div class="form-group row">
								<label class="col-lg-2 col-form-label">Nama Penuh</label>
								<div class="col-lg-10">
									<input type="text" placeholder="Sila masukkan nama anda" class="form-control" name="nama" value="{{ old('nama') ?? '' }}"> 
									<span class="form-text m-b-none">Masukkan nama mengikut MyKad</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-2 col-form-label">E-mel</label>
								<div class="col-lg-10">
									<input type="email" placeholder="contoh@intan.my" class="form-control" name="emel" value="{{ old('emel') ?? '' }}"> 
									<span class="form-text m-b-none">Sila masukkan e-mel rasmi anda</span>
								</div>
							</div>
							<div class="form-group row"><label class="col-lg-2 col-form-label">Katalaluan</label>

								<div class="col-lg-10">
									<input type="password" placeholder="Masukkan katalaluan" class="form-control" name="katalaluan" value="{{ old('katalaluan' ?? '') }}">
								</div>
							</div>
							<!-- <div class="form-group row">
								<div class="col-lg-offset-2 col-lg-10">
									<div class="i-checks"><label> <input type="checkbox"> Remember me </label></div>
								</div>
							</div> -->
							<div class="form-group row">
								<div class="col-lg-offset-2 col-lg-12">
									<button class="btn btn-lg btn-success btn-block" type="submit">Daftar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<script type="text/javascript">
		$("document").ready(function(){
			$(".alert").fadeTo(2000, 500).slideup(500, function(){
				$(".alert").slideup(500);
			});
		});
	</script>
@endsection
@push('script')
<script type="text/javascript">
	$("document").ready(function(){
		$(".alert").fadeTo(2000, 500).slideup(500, function(){
			$(".alert").slideup(500);
		});
	});
</script>
@endpush
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PesertaController;
//use App\Http\Controllers\Api_Controllers\NamaController jika ada subfolder dlm folder Controllers

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
//             //folder //nama file welcome.blade.php
//             //view        
// });

// Route::get('/login', function(){
//     return view('welcome');
// })->name('login');

Route::get('/register/pengguna/pengesahan', function(){
    return view('welcome');
})->name('registerd'); //nama perlu mudah difahami

Route::get('/', [HomeController::class,'index'])->name('utama');

Route::get('/master/template', [HomeController::class, 'master_template'])->name('master.template'); //master_template adalah nama function dalam HomeController

Route::get('/login', function(){
    return view('login');
})->name('login');

Route::get('/login/template', function(){
    return view('login_template');
})->name('login.template');

Route::get('/daftar/pengguna', function(){
    return view('pengguna.borang_daftar');
})->name('borang.daftar');

Route::post('/simpan/maklumat/pengguna', [PesertaController::class, 'simpan_maklumat'])->name('simpan.maklumat');

Route::post('/pengesahan/pengguna',[PesertaController::class, 'authenticate'])->name('pengesahan.pengguna');

Route::group(['middleware' => ['auth']], function(){
    Route::get('/dashboard/pengguna', function(){
        return view('dashboard');
    })->name('dashboard');

    Route::get('/senarai/peserta', [PesertaController::class, 'senarai_pengguna'])->name('senarai.pengguna');

    Route::get('/kemaskini/pengguna/{id}', [PesertaController::class, 'kemaskini_pengguna'])->name('edit.pengguna');

    Route::get('/logout', [PesertaController::class, 'logout'])->name('logout');
});
